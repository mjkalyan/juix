(define-module (james home services theme)
  #:use-module (gnu home services)
  #:use-module (gnu home services shepherd)
  #:use-module (gnu services configuration)
  #:use-module (guix gexp)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:export (home-theme-configuration
            home-theme-service-type))

(define-configuration/no-serialization home-theme-configuration
  (procedures
   (list '()) ;  e.g. '((modus-operandi . ((lambda () (emacs-load-theme 'modus-operandi))
              ;                            (lambda () (alacritty-change-symlink 'modus-operandi)))))
   "An association list of theme names & their respective theme change thunks."))

(define (theme-shepherd-services config)
  (list
   (shepherd-service
    (provision '(theme))
    (actions (list
              (shepherd-action
               (name 'switch)
               (documentation "Switch the theme for all supported programs.")
               (procedure
                ;; ARGS are the same ARGS provided to the herd command
                ;; e.g. 'herd switch theme modus-operandi [ARG...]'
                #~(lambda (running new-theme . _args)
                    (if running
                        ;; Run all the given procedures associated with the new-theme
                        (for-each (lambda (x) (x))
                                  (assoc-ref #$(home-theme-configuration-procedures config)
                                             new-theme))))))))
    (documentation "A way to switch themes for all supported programs."))))

(define home-theme-service-type
  (service-type
   (name 'home-theme)
   (extensions (list (service-extension home-shepherd-service-type
                                        theme-shepherd-services)))
   (compose concatenate)
   ;; Extending this means adding procedures for changing themes within a program
   (extend (lambda (config new-procedures)
             (if (null? new-procedures)
                 config
                 (match config
                   (($ <home-theme-configuration> initial-procedures)
                    (home-theme-configuration
                     (procedures (match-let merge-alists
                                   ((acc initial-procedures)
                                    ((merging . remaining) new-procedures)
                                    ((theme-name . thunks) (car new-procedures)))
                                   (if (null? remaining)
                                       acc
                                       (merge-alists (assoc-set! initial-procedures
                                                                 theme-name
                                                                 (append thunks
                                                                         (assoc-ref initial-procedures theme-name)))
                                                     remaining
                                                     (car remaining)))))))))))
   (default-value (home-theme-configuration))
   (description "Provides a Shepherd service to change software themes on the fly à la Pywal")))
