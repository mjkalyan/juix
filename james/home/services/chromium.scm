(define-module (james home services chromium)
  #:use-module (gnu home services)
  #:use-module (gnu home services shepherd)
  #:use-module (gnu services configuration)
  #:use-module (gnu packages chromium)
  #:use-module (guix packages)
  #:use-module (guix records)
  #:use-module (guix gexp)
  #:use-module (james packages browser-extensions)
  #:use-module (james home services theme)
  #:use-module (ice-9 textual-ports)
  #:export (home-chromium-configuration
            home-chromium-service-type))

;; home-chromium-service-type will do the following:
;; - Install ungoogled-chromium
;; - (optional) Install ublock-origin-chromium
;; - (optional) Install duckduckgo-settings-manager-chromium
;; - (optional) Register/install ddg-settings-messaging-host.scm at ~/.config/chromium/NativeMessagingHosts/ddg-settings-messaging-host.json
;;   TODO I think the gexp will install ddg-settings-messaging-host and we don't need/want it in the profile so we don't extend home-profile
;; - (optional) Extend the theme switcher service for duckduckgo (and chromium?)

(define-configuration/no-serialization home-chromium-configuration
  (chromium
   (package ungoogled-chromium/wayland)
   "The Chromium package to install.")
  (ublock-origin?
   (boolean #t)
   "Whether to install the uBlock Origin adblocker extension.")
  (theme
   (symbol 'modus-operandi)
   "The theme to use for Chromium & DuckDuckGo.")
  (ddg-settings-manager?
   (boolean #f)
   "Whether to use a Chromium extension & theme switcher service extension to change DuckDuckGo settings on the fly.")
  ;; TODO: implement a way to set these fonts in Chromium. Possibly
  ;; done by installing/modifying the
  ;; $XDG_CONFIG_HOME/chromium/Default/Preferences json file; we can
  ;; find the font fields in the webkit.webprefs.fonts object. If this
  ;; works, other chromium preferences should be modifiable here :D
  ;; (font-size
  ;;  (number 16) ; ∈ [9 - 72]
  ;;  "The Chromium font size.")
  ;; (fixed-font-size
  ;;  (number 13) ; ∈ [9 - 72]
  ;;  "The Chromium fixed-width font size.")
  ;; (minimum-font-size
  ;;  (number 12) ; ∈ [0 - 24]
  ;;  "The minimum Chromium font size.")
  ;; (standard-font
  ;;  (string "Iosevka Comfy Motion Duo")
  ;;  "The standard Chromium font.")
  ;; (serif-font
  ;;  (string "Cormorant Garamond")
  ;;  "The serif Chromium font.")
  ;; (sans-serif-font
  ;;  (string "Iosevka Comfy Duo")
  ;;  "The sans-serif Chromium font.")
  ;; (fixed-width-font
  ;;  (string "Iosevka Comfy Motion")
  ;;  "The fixed-width Chromium font.")
  ;; (math-font
  ;;  string
  ;;  "The mathematical Chromium font.")
  )

(define (chromium-profile config)
  (let* ((packages (list (home-chromium-configuration-chromium config)))
         (packages (if (home-chromium-configuration-ublock-origin? config)
                       (cons ublock-origin-chromium packages)
                       packages))
         (packages (case (home-chromium-configuration-theme config)
                     ((modus-operandi) (cons modus-operandi-chromium packages))
                     ((modus-vivendi) (cons modus-vivendi-chromium packages))
                     ((modus-operandi-tinted) (cons modus-operandi-tinted-chromium packages))
                     ((modus-vivendi-tinted) (cons modus-vivendi-tinted-chromium packages))
                     (else packages)))
         (packages (if (home-chromium-configuration-ddg-settings-manager? config)
                       packages ;; TODO replace with: (cons duckduckgo-settings-manager-chromium packages)
                       packages)))
    packages))

(define messaging-host-manifest
  (plain-file "ddg-settings-messaging-host.json"
              (string-append "{
  \"name\": \"ddg-settings-messaging-host\",
  \"description\": \"Relay DuckDuckGo setting changes to DuckDuckGo Settings Manager.\",
  \"path\": " #|TODO uncomment: (file-append ddg-settings-messaging-host "/bin/ddg-settings-messaging-host")|# ",
  \"type\": \"stdio\",
  \"allowed_origins\": [\"chrome-extension://eakcnmcbbjgdjpkmnnebmhebhgjahajh/\"]
}")))

(define (chromium-xdg-config-files config)
  `(("chromium/NativeMessagingHosts/ddg-settings-messaging-host.json" messaging-host-manifest)
    ;; ("chromium/Default/Preferences" ...) ; TODO don't think this is possible, chromium will revert manual changes
    ))

(define (chromium-theme-procedures config)
  (define (make-send-msg-thunk theme)
    (lambda ()
      ;; Send message via named pipe to the native messaging host
      (call-with-output-file "/tmp/ddg-settings-messaging-host.fifo"
        (lambda (port)
          (format port "~a\n" theme)))))
  (if (home-chromium-configuration-ddg-settings-manager? config)
      `((modus-operandi . ,(make-send-msg-thunk 'modus-operandi))
        (modus-vivendi . ,(make-send-msg-thunk 'modus-vivendi))
        (modus-operandi-tinted . ,(make-send-msg-thunk 'modus-operandi-tinted))
        (modus-vivendi-tinted . ,(make-send-msg-thunk 'modus-vivendi-tinted)))))

(define home-chromium-service-type
  (service-type
   (name 'home-chromium)
   (extensions
    (list (service-extension home-profile-service-type
                             chromium-profile)
          (service-extension home-xdg-configuration-files-service-type
                             chromium-xdg-config-files)
          (service-extension home-theme-service-type
                             chromium-theme-procedures)))
   (default-value (home-chromium-configuration))
   (description "Installs & configures Chromium, including extensions and themes.")))
