(define-module (james home services desktop)
  #:use-module (gnu home services)
  #:use-module (gnu home services shepherd)
  #:use-module (gnu services configuration)
  #:use-module (gnu packages wm)
  #:use-module (guix records)
  #:use-module (guix gexp)
  #:export (home-mako-configuration
            home-mako-service-type))

(define-maybe file-like)

;; TODO should be a configuration record (see define-configuration/no-serialization)
(define-configuration home-mako-configuration
  (config
   maybe-file-like
   "The mako config file to be installed."))

(define (mako-shepherd-services _config)
  (list (shepherd-service
         (provision '(mako notification-daemon))
         (requirement '(dbus))
         (start #~(make-forkexec-constructor
                   '(#$(file-append mako "/bin/mako"))
                   #:log-file ".local/state/log/mako.log"
                   #:environment-variables (cons #$(string-append "WAYLAND_DISPLAY=" (getenv "WAYLAND_DISPLAY"))
                                                 (default-environment-variables))))
         (stop #~(make-kill-destructor))
         (respawn? #t)
         (documentation "The Mako notification daemon"))))

(define (mako-profile _config)
  (list mako))

(define (mako-config config)
  "Produce the mako config file from the given home-mako-configuration record."
  (let ((mako-config (home-mako-configuration-config config)))
    (if mako-config
        mako-config
        ;; for now, just fall back on an empty file-like:
        (plain-file "config" ""))))

(define (mako-xdg-config-files config)
  ;; TODO use empty list here when no mako config is given, i.e. do
  ;; not install any configuration file
  `(("mako/config" ,(mako-config config))))

(define home-mako-service-type
  (service-type
   (name 'home-mako)
   (extensions
    (list (service-extension home-shepherd-service-type mako-shepherd-services)
          (service-extension home-profile-service-type mako-profile)
          (service-extension home-xdg-configuration-files-service-type mako-xdg-config-files)))
   (default-value (home-mako-configuration))
   (description "Configures the Mako notification daemon for Guix Home usage.")))


;; TODO
;; (define home-fcitx5-service-type
;;   (service-type
;;    (name 'home-fcitx5)
;;    (extensions
;;     (list (service-extension home-environment-variables-service-type
;;                              `(("XMODIFIERS" . "@im=fcitx") ;; TODO rm, only needed for X applications
;;                                ("QT_IM_MODULE"  . "fcitx")
;;                                ("QT_IM_MODULES" . "wayland;fcitx")))
;;           (service-extnesion home-profile-service-type
;;                              (list fcitx5
;;                                    fcitx5-rime
;;                                    fcitx5-configtool))))))
