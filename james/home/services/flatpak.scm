(define-module (james home services flatpak)
  #:use-module (gnu home services)
  #:use-module (gnu services configuration)
  #:use-module (gnu packages package-management)
  #:use-module (guix records)
  #:use-module (guix gexp)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 popen)
  #:use-module (ice-9 textual-ports)
  #:use-module (ice-9 match)
  #:export (home-flatpak-configuration
            home-flatpak-service-type))

(define-configuration/no-serialization home-flatpak-configuration
  (apps
   (list '())
   "A list of Flatpak application ID strings & optional Flatpak overrides (file-like objects) that will be installed.")
  (augment-xdg-data-dirs?
   (boolean #t)
   "Whether to include Flatpak's application data (desktop files, etc.) in XDG_DATA_DIRS.")
  (auto-update?
   (boolean #f)
   "Whether to update Flatpak applications automatically whenever this service is booted.")
  (cull-undeclared?
   (boolean #f)
   "Whether to uninstall Flatpak applications if they are not declared in the `apps' field."))

(define (flatpak-environment-variables config)
  (if (home-flatpak-configuration-augment-xdg-data-dirs? config)
      '(("XDG_DATA_DIRS" . "$XDG_DATA_DIRS:$XDG_DATA_HOME/flatpak/exports/share"))
      '()))

(define (flatpak-profile config)
  (list flatpak))

(define (flatpak-activation config)
  (let* ((apps (map car (home-flatpak-configuration-apps config)))
         (fp (file-append flatpak "/bin/flatpak")))
    #~(begin
        (use-modules (ice-9 popen)
                     (ice-9 textual-ports))
        #$(when (home-flatpak-configuration-cull-undeclared? config)
            #~(let* ((lines (compose (lambda (x) (string-split x #\newline))
                                     (lambda (x) (string-trim-right x #\newline))
                                     get-string-all))
                     (p (open-pipe* OPEN_READ #$fp "list" "--app" "--columns=application"))
                     (installed (lines p))
                     (_ (close-pipe p))
                     (undeclared (filter (lambda (x) (not (member x '#$apps)))
                                         installed)))
                (display "Removing undeclared Flatpak applications...\n")
                (unless (null? undeclared)
                  (apply system* #$fp "remove" "-y" "--noninteractive" undeclared))))
        #$(when (home-flatpak-configuration-auto-update? config)
            #~(system* #$fp "update" "-y" "--noninteractive"))
        #$(unless (null? apps)
            #~(system* #$fp "install" "-y" "--noninteractive" #$@apps)))))

;; TODO use XDG_DATA_DIR/flatpak/overrides
(define flatpak-overrides-directory ".local/share/flatpak/overrides")

(define (flatpak-files config)
  (filter-map (match-lambda
                ((id . ()) #f) ; Just an id, no overrides
                ((id overrides)
                 (list (string-append flatpak-overrides-directory "/" id)
                       overrides)))
              (home-flatpak-configuration-apps config)))

(define home-flatpak-service-type
  (service-type
   (name 'home-flatpak)
   (extensions
    (list (service-extension home-environment-variables-service-type
                             flatpak-environment-variables)
          (service-extension home-profile-service-type
                             flatpak-profile)
          (service-extension home-activation-service-type
                             flatpak-activation)
          (service-extension home-files-service-type
                             flatpak-files)))
   (default-value (home-flatpak-configuration))
   (description "Installs Flatpak and extends XDG_USER_DIRS to search for Flatpak's desktop files.")))
