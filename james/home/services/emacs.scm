(define-module (james home services emacs)
  #:use-module (gnu home services)
  #:use-module (gnu home services shepherd)
  #:use-module (gnu services configuration)
  #:use-module (gnu packages emacs)
  #:use-module (guix packages)
  #:use-module (guix records)
  #:use-module (guix gexp)
  #:export (home-emacs-configuration
            home-emacs-service-type))


(define-configuration/no-serialization home-emacs-configuration
  (emacs
   (package emacs-next-pgtk)
   "The emacs package to install (e.g. emacs or emacs-next-pgtk).")
  (emacs-server?
   (boolean #f)
   "Whether to start the Emacs server as a Shepherd service.")
  (init-file
   (text-config '())
   "A list of file-like objects which will be added to @file{init.el}.")
  (emacs-packages
   (list-of-packages '())
   "A list of packages to install with/for Emacs."))

(define (emacs-profile config)
  (cons (home-emacs-configuration-emacs config)
        (home-emacs-configuration-emacs-packages config)))

(define (emacs-config-files config)
  `(("emacs/init.el" ,(home-emacs-configuration-init-file config))))

(define (emacs-shepherd-service config)
  (list (shepherd-service
         (provision '(emacs))
         (start #~(make-forkexec-constructor '("emacs" "--fg-daemon")))
         (stop #~(make-kill-destructor))
         (respawn? #f)
         (documentation "GNU Emacs Server"))))

(define home-emacs-service-type
  (service-type
   (name 'home-emacs)
   (extensions
    (list (service-extension home-profile-service-type
                             emacs-profile)
          (service-extension home-xdg-configuration-files-service-type
                             emacs-config-files)
          (service-extension home-shepherd-service-type
                             emacs-shepherd-service)))
   (default-value (home-emacs-configuration))
   (description "Installs & configures GNU Emacs, potentially managing the Emacs server as a Shepherd service.")))
