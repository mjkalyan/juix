(define-module (james home services mpd)
  #:use-module (gnu home services)
  #:use-module (gnu home services shepherd)
  #:use-module (gnu services configuration)
  #:use-module (gnu packages mpd)
  #:use-module (guix records)
  #:use-module (guix gexp)
  #:export (home-mpd-configuration
            home-mpd-service-type))

(define-maybe file-like)

;; TODO should be a configuration record (see define-configuration/no-serialization)
(define-configuration home-mpd-configuration
  (config
   maybe-file-like
   "The mpd.conf file to be installed."))

(define (mpd-shepherd-services config)
  (list (shepherd-service
         (provision '(mpd))
         (requirement '(pipewire pipewire-pulseaudio))
         (start #~(make-forkexec-constructor '(#$(file-append mpd "/bin/mpd") "--no-daemon")))
         (stop #~(make-kill-destructor))
         (respawn? #f)
         (documentation "The Music Player Daemon"))))

(define (mpd-profile config)
  (list mpd))

(define (mpd-conf config)
  "Produce the mpd.conf file from the given home-mpd-configuration record."
  (home-mpd-configuration-config config))

(define (mpd-xdg-config-files config)
  ;; TODO use empty list here when no mpd.conf is given
  `(("mpd/mpd.conf" ,(mpd-conf config))))

(define home-mpd-service-type
  (service-type
   (name 'home-mpd)
   (extensions
    (list (service-extension home-shepherd-service-type
                             mpd-shepherd-services)
          (service-extension home-profile-service-type
                             mpd-profile)
          (service-extension home-xdg-configuration-files-service-type
                             mpd-xdg-config-files)))
   (default-value (home-mpd-configuration))
   (description "Configures the Music Player Daemon (MPD) for use as a Shepherd service under Guix Home.")))
