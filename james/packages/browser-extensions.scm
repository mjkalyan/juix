(define-module (james packages browser-extensions)
  #:use-module (gnu build chromium-extension)
  #:use-module (guix build-system copy)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix licenses)
  #:use-module (guix packages))


(define modus-operandi
  (package
   (name "modus-operandi")
   (version "3.0")
   (home-page "https://gitlab.com/mjkalyan/modus-operandi-chromium")
   (source (origin
            (method url-fetch)
            (uri (string-append "https://gitlab.com/mjkalyan/" name "-chromium/-/archive/"
                                version "/" name "-chromium-" version ".tar.gz"))
            (sha256
             (base32
              "1jxhayc1ixnm5vas1w2kpl4jqb273zyyvgkhx9441i5n8pay6d3i"))))
   (build-system copy-build-system)
   (synopsis "The modus-operandi theme for Chromium.")
   (description "A theme for Chromium based on modus-operandi from Protesilaos
Stavrou's Modus Themes for Emacs.")
   (license expat)))

(define-public modus-operandi/chromium
  (make-chromium-extension modus-operandi))


(define modus-operandi-tinted
  (package
   (name "modus-operandi-tinted")
   (version "3.0")
   (home-page "https://gitlab.com/mjkalyan/modus-operandi-tinted-chromium")
   (source (origin
            (method url-fetch)
            (uri (string-append "https://gitlab.com/mjkalyan/" name "-chromium/-/archive/"
                                version "/" name "-chromium-" version ".tar.gz"))
            (sha256
             (base32
              "18kh6ciwgaizikclb46cli29cw5mm4ifcy8vhi19ws63s5xa9bh7"))))
   (build-system copy-build-system)
   (synopsis "The modus-operandi-tinted theme for Chromium.")
   (description "A theme for Chromium based on modus-operandi-tinted from
Protesilaos Stavrou's Modus Themes for Emacs.")
   (license expat)))

(define-public modus-operandi-tinted/chromium
  (make-chromium-extension modus-operandi-tinted))


(define modus-vivendi
  (package
   (name "modus-vivendi")
   (version "3.0")
   (home-page "https://gitlab.com/mjkalyan/modus-vivendi-chromium")
   (source (origin
            (method url-fetch)
            (uri (string-append "https://gitlab.com/mjkalyan/" name "-chromium/-/archive/"
                                version "/" name "-chromium-" version ".tar.gz"))
            (sha256
             (base32
              "0s2dlwrjll0gc5aj2x3y07hvj62984ljrbh71dl1dbhwdgsyzzn7"))))
   (build-system copy-build-system)
   (synopsis "The modus-vivendi theme for Chromium.")
   (description "A theme for Chromium based on modus-vivendi from Protesilaos
Stavrou's Modus Themes for Emacs.")
   (license expat)))

(define-public modus-vivendi/chromium
  (make-chromium-extension modus-vivendi))


(define modus-vivendi-tinted
  (package
   (name "modus-vivendi-tinted")
   (version "3.0")
   (home-page "https://gitlab.com/mjkalyan/modus-vivendi-tinted-chromium")
   (source (origin
            (method url-fetch)
            (uri (string-append "https://gitlab.com/mjkalyan/" name "-chromium/-/archive/"
                                version "/" name "-chromium-" version ".tar.gz"))
            (sha256
             (base32
              "1vckc07qaz4ip6q1q8fnjyj67nvllmbh81byhar26w5cxb25m978"))))
   (build-system copy-build-system)
   (synopsis "The modus-vivendi-tinted theme for Chromium.")
   (description "A theme for Chromium based on modus-vivendi-tinted from Protesilaos
Stavrou's Modus Themes for Emacs.")
   (license expat)))

(define-public modus-vivendi-tinted/chromium
  (make-chromium-extension modus-vivendi-tinted))


;; (define duckduckgo-settings-manager
;;   (package
;;    (name "duckduckgo-settings-manager")
;;    (version "1.0")
;;    (home-page "https://gitlab.com/mjkalyan/TODO")
;;    (source (origin
;;             (method url-fetch)
;;             (uri (string-append "https://gitlab.com/mjkalyan/" name "-chromium/-/archive/"
;;                                 version "/" name "-chromium-" version ".tar.gz"))
;;             (sha256
;;              (base32
;;               "TODO"))))
;;    (build-system copy-build-system)
;;    (synopsis "TODO")
;;    (description "TODO")
;;    (license expat)))

;; (define-public duckduckgo-settings-manager/chromium
;;   (make-chromium-extension duckduckgo-settings-manager))

;; TODO guile script
;; (define-public ddg-settings-messaging-host
;;   (package
;;    ))
