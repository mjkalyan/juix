(define-module (james packages misc)
  #:use-module (gnu build chromium-extension)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages emacs-xyz)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system emacs)
  #:use-module (guix build-system guile)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix licenses)
  #:use-module (guix packages))


(define-public guile-xdg-base-dir
  (package
   (name "guile-xdg-base-dir")
   (version "1.0")
   (source (origin
            (method url-fetch)
            (uri (string-append "https://gitlab.com/mjkalyan/" name "/-/archive/" version
                                "/" name "-" version ".tar.gz"))
            (sha256
             (base32
              "1g4ra205w827rlp3xqg0xwsbzxp6j9zr6i6g3pqrx118lrcx3x41"))))
   (build-system guile-build-system)
   (inputs (list guile-3.0-latest))
   (native-inputs (list guile-3.0-latest))
   (synopsis "XDG Base Directory specification in Guile Scheme")
   (description "Provides Guile Scheme procedures for accessing user-defined or the
default XDG Base Directory locations.")
   (home-page "https://gitlab.com/mjkalyan/guile-xdg-base-dir")
   (license gpl3+)))


(define-public cache-livestreams
  (package
   (name "cache-livestreams")
   (version "1.1")
   (source (origin
            (method url-fetch)
            (uri (string-append "https://gitlab.com/mjkalyan/" name "/-/archive/" version
                                "/" name "-" version ".tar.gz"))
            (sha256
             (base32
              "1qar38nrr1zc33qmdx40aw3c7vkrr7zdkvyb7jnm5mixg4yni5ld"))))
   (build-system guile-build-system)
   (inputs (list guile-3.0-latest))
   (native-inputs (list guile-3.0-latest))
   (propagated-inputs (list guile-xdg-base-dir))
   (synopsis "Cache a list of online livestreamers")
   (description "Find out which users on your list, located at
$XDG_DATA_HOME/cache-livestreams/following, is livestreaming and cache the results
in $XDG_CACHE_HOME/livestreams.")
   (home-page "https://gitlab.com/mjkalyan/cache-livestreams")
   (license gpl3+)))


(define-public guile-colour-scheme
  (package
   (name "guile-colour-scheme")
   (version "3.1")
   (source (origin
            (method url-fetch)
            (uri (string-append "https://gitlab.com/mjkalyan/" name "/-/archive/" version
                                "/" name "-" version ".tar.gz"))
            (sha256
             (base32
              "1g2r4vrhmqzm3kd79g6vp6ryjz04ylkic2w6lzyrcfmfzl31kka5"))))
   (build-system guile-build-system)
   (inputs (list guile-3.0-latest))
   (native-inputs (list guile-3.0-latest))
   (synopsis "Retrieve colours from Emacs-like colour schemes in Guile Scheme")
   (description "Given the name of a colour (such as fg-main), get the corresponding
string representation in guile.")
   (home-page "https://gitlab.com/mjkalyan/colour-scheme")
   (license expat)))


(define-public emacs-flycheck-hledger
  (package
   (name "emacs-flycheck-hledger")
   (version "1.0.0")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://github.com/DamienCassou/flycheck-hledger")
                  (commit (string-append "v" version))))
            (file-name (git-file-name name version))
            (sha256
             (base32
              "1viwyphz1pz6hhr3g2w96pza3b6vkyd2izz4jr5q6787i1n0yb8d"))))
   (build-system emacs-build-system)
   (propagated-inputs
    (list emacs-flycheck))
   (home-page "https://github.com/DamienCassou/flycheck-hledger")
   (synopsis "hledger checker for flycheck")
   (description
    "An Emacs syntax checker for hledger/ledger journals using flycheck.")
   (license gpl3+)))
